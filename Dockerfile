from debian:jessie

RUN echo deb http://www.deb-multimedia.org jessie main non-free >> /etc/apt/sources.list && echo deb-src http://www.deb-multimedia.org jessie main non-free >> /etc/apt/sources.list
RUN apt-get update && apt-get install -qq --force-yes --no-install-recommends deb-multimedia-keyring && apt-get clean
RUN apt-get update && apt-get install -y -qq --no-install-recommends build-essential texi2html chrpath git gawk python texinfo diffstat wget ca-certificates cpio git vim imagemagick python3 librsvg2-bin xsltproc gettext zip xfonts-utils ffmpeg bash && apt-get clean
RUN useradd openvario
COPY openvario openvario
RUN rm -f /bin/sh && ln -s /bin/bash /bin/sh
RUN cd /openvario && MACHINE=openvario-7-CH070 ./oebb.sh config openvario-7-CH070
RUN chown -R openvario /openvario
RUN cd openvario && su openvario -c 'source ./environment-angstrom-v2014.12 && bitbake xcsoar' && rm -rf /openvario/sources/downloads/git2
ENTRYPOINT /bin/bash
